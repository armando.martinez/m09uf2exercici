﻿using System;
using System.Threading;

namespace M09UF2Exercici
{
    class Program
    {
        // No he acabado de entender el punto del ejercicio? Quizá me perdí algo en clase
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Nevera n = new Nevera();

            Thread t4 = new Thread(() =>
            {
                n.BeureCervesa("Manuel Turizo");
            });

            Thread t3 = new Thread(() =>
            {
                n.BeureCervesa("Lil Nas X");
            });

            Thread t2 = new Thread(() =>
            {
                n.BeureCervesa("Bad Bunny");
            });

            Thread t = new Thread(() =>
            {
                n.OmplirNevera("Anitta");
            });

            t.Start();
            t2.Start();
            t3.Start();
            t4.Start();
        }
    }
}
