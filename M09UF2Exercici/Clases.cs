﻿using System;
class Nevera
{
    public int cervezas = 0;
    private readonly Random rnd = new Random();

    public Nevera() { }

    public void OmplirNevera(string nom)
    {
        int n = rnd.Next(0, 6);
        cervezas += n;
        Console.WriteLine($"{nom} llena la nevera con {n} cervezas");
    }
    public void BeureCervesa(string nom)
    {
        int n = rnd.Next(0, 6);
        cervezas -= n;
        Console.WriteLine($"{nom} bebe {n} cerveza(s)");
    }
}